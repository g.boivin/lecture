import React from 'react';
import CleSol from './sol.svg';
import CleFa from './fa.svg';

import './sheet.css';

const cles = {
    sol: CleSol,
    fa: CleFa,
};

export default class Sheet extends React.Component {
    render() {
        const cle = this.props.cle;
        return <div className='sheet' data-cle={cle}>
            {this.props.notes}
            <img alt={`cle de ${cle}`} className={`cle-${cle}`} src={cles[cle]}/>
            <div className='line'></div>
            <div className='line'></div>
            <div className='line'></div>
            <div className='line'></div>
            <div className='line'></div>
        </div>
    }
}