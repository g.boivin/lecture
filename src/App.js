import React, { Component } from 'react';
import Loading from './Loading';
import Sheet from './Sheet';
import Music from './Music';
import Input from './Input';
import Init from './Init';
import CleSol from './sol.svg';
import CleFa from './fa.svg';

import './App.css';

const cles = {
    sol: CleFa,
    fa: CleSol,
};

const cle = 'sol';
const difficulties = ['easy', 'medium', 'hard'];
const difficulty = difficulties[0];

window.difficulty = difficulty;
const renderScore = () => <div className='score'>{`${window.score}/${window.originalMusic.length}`}</div>

class App extends Component {
  constructor() {
    super();
    this.switchDifficulty = this.switchDifficulty.bind(this);
    this.state = {
      cle,
      difficulty,
    };
  }

  switchDifficulty() {
      const currentIndex = difficulties.indexOf(this.state.difficulty);
      window.difficulty =  currentIndex === difficulties.length -1 ? difficulties[0] : difficulties[currentIndex + 1];
      Init(() => {
        this.setState({loaded: true, music: window.music});
      });
      this.setState({difficulty: window.difficulty});
  }

  componentDidMount() {
    Init(() => {
      setTimeout(() => this.setState({loaded: true, music: window.music}), 900);
    });
  }

  render() {

    if (window.music && window.music.length === 0) {
      document.body.addEventListener('click', () => Init(() => {
        this.setState({ music: window.music });
      }), { once: true });
      return <div className="App results"><div className='bravo'>Bravo!</div>{renderScore()}</div>
    }

    return (
      <div className="App">
        {!this.state.loaded && <Loading/>}
        {this.state.loaded && <span className={`niveau-switch switch`} onClick={this.switchDifficulty}>{this.state.difficulty}</span>}
        {this.state.loaded && <img className={`cle-switch switch ${this.state.cle === 'sol' ? 'fa' : 'sol'}`} onClick={() => this.setState({cle: this.state.cle === 'sol' ? 'fa' : 'sol'})} src={cles[this.state.cle]} alt={this.state.cle}/>}
        {this.state.loaded && renderScore()}
        {this.state.loaded && <Sheet cle={this.state.cle} notes={<Music difficulty={this.state.difficulty} cle={this.state.cle} music={this.state.music}/>}/>}
        {this.state.loaded && <Input app={this}/>}
      </div>
    );
  }
}

export default App;
