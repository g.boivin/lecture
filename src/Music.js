import React from 'react';
import Note from './Note';

export default ({ music, cle, difficulty}) => <div className='music'>
    {music.map((n, i) => <Note key={i} difficulty={difficulty} cle={cle} note={n} i={i}/>)}
</div>