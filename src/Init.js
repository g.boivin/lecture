export const notes = ['do', 're','mi', 'fa', 'sol', 'la', 'si'];
export const pitches = [0, 1, 2];
const difficulties = ['easy', 'medium', 'hard'];


class Note {
    constructor(note, pitch) {
        this.note = note;
        this.pitch = pitch;
    }

    toString() {
        return `${this.note}/${this.pitch}`;
    }
}

const randomNote = () => {
    return new Note(notes[Math.floor((Math.random() * notes.length))], pitches[Math.floor((Math.random() * (difficulties.indexOf(window.difficulty) + 1)))])
}

export default (callback) => {
    const randomMusic = () => {
        const random = [];
        for (let i = 1; i <= 10; i++) {
            random.push(randomNote());
        }
        return random;
    }
    const gamme = [];
    for (let j = 0; j < difficulties.indexOf(window.difficulty) + 1; j++) {
        for (let i = 0; i < 7; i++) {
            gamme.push(new Note(notes[i], pitches[j]))
        }
    }

    //window.music = gamme;
    window.music = randomMusic();
    window.originalMusic = [...window.music];
    window.score = 0;
    callback();
};



