import React from 'react';
import { notes } from './Init';

const onClick = (text, app) => {
    if (app.state.music[0].note === text) {
      window.score++;
    }
    window.music.splice(0, 1);
    app.setState({music: window.music});
};

export default ({ app }) => <div className='note-input'>
  {notes.concat('do').map(note => <div onClick={({ target }) => onClick(target.textContent, app)}>{note}</div>)}
</div>
/**
export default ({ app }) => <input type="text" autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="false" onKeyUp={({ keyCode, target })=> {
    if (keyCode === 13) {
      if (app.state.music.indexOf(target.value.toLowerCase()) === 0) {
        window.score++;
      }
      target.value = '';
      window.music.splice(0, 1);
      app.setState({music: window.music});
    }
  }}/>

  */