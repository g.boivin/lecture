import React, { Component } from 'react';
import NoteSVG from './note.svg';

const notes = ['do', 're', 'mi', 'fa', 'sol', 'la', 'si'];

const basePosition = {
    sol: -52,
    fa: -7,
};

const pitches = {
    0: 0,
    1: -63,
    2: 63,
};

export default class Note extends Component {
    isHigh() {
        return this.props.note.pitch === 2 || (notes.indexOf(this.props.note.note) > 5 && this.props.note.pitch === 0)
    }
    position() {
        let bottom = basePosition[this.props.cle] + 9 * (notes.indexOf(this.props.note.note) + 5) + pitches[this.props.note.pitch];
        const left = 85 + (45 * this.props.i);
        if (this.isHigh()) bottom -= 34;
        return { left, bottom };
    }
    render() {
        return <div className={`note ${this.isHigh() ? 'high' : ''}`} data-note={this.props.note.note} style={this.position()}><img alt={this.props.note} src={NoteSVG}/>
        </div>
    }
};

