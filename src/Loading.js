import React from 'react';

export default () => <div className='loading'><div>♬</div><span>...</span></div>